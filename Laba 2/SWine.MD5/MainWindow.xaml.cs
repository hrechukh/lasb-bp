﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace SWine.MD5
{
    public partial class MainWindow
    {
        private string _filepath = string.Empty;
        private string _hashCmpValue = string.Empty;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnFileSelectButtonClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Title = "Select file...",
                Multiselect = false
            };
            if (dialog.ShowDialog(this) == true)
            {
                _filepath = dialog.FileName;
                TextBoxFileDestination.Text = _filepath;
            }
        }

        private void OnCompareButtonClick(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                CheckPathExists = true,
                Title = "Select hash file...",
                Multiselect = false,
                Filter = "Hash files (*.hash)|*.hash|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog(this) == true)
            {
                using (var streamReader = new StreamReader(dialog.FileName))
                {
                    _hashCmpValue = streamReader.ReadLine();
                }
                if (string.IsNullOrWhiteSpace(_filepath))
                    throw new Exception("No file");
                var button = ButtonStart;
                button.IsEnabled = false;
                ThreadPool.QueueUserWorkItem(delegate
                {
                    using (var stream =
                        new BufferedStream(new FileStream(_filepath, FileMode.Open, FileAccess.Read))
                    )
                    {
                        var digest = Md5.Calculate(stream);
                        Dispatcher.Invoke(delegate
                        {
                            ResultText.Text += "\nHash to compare:\t" + _hashCmpValue +
                                                    "\nThe resulting hash file:\t" +
                                                    digest + "\nResult:\t" +
                                                    (_hashCmpValue.Equals(digest.ToString())
                                                        ? "Match"
                                                        : "Don't match");
                        });
                        Dispatcher.Invoke(delegate { button.IsEnabled = true; });
                    }
                });
            }
            else
            {
                MessageBox.Show("Select the correct hash file");
            }
        }

        private void OnCalculateButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var hashStoreFlag = CheckBoxHashStore.IsChecked != null && CheckBoxHashStore.IsChecked.Value;
                var button = sender as Button;
                if (RadioButtonFileSourceSelector.IsChecked != null &&
                    RadioButtonFileSourceSelector.IsChecked.Value)
                {
                    if (string.IsNullOrWhiteSpace(_filepath))
                        throw new Exception("File not checked");
                    if (button == null) return;
                    button.IsEnabled = false;
                    ThreadPool.QueueUserWorkItem(delegate
                    {
                        using (
                            var stream =
                                new BufferedStream(new FileStream(this._filepath, FileMode.Open, FileAccess.Read))
                        )
                        {
                            var digest = Md5.Calculate(stream);
                            this.Dispatcher.Invoke(delegate { this.ResultText.Text += "\n Hash: " + digest; });
                            if (hashStoreFlag)
                                using (
                                    var storeStream =
                                        new StreamWriter(
                                            new FileStream(
                                                this._filepath.Substring(0, this._filepath.LastIndexOf(".", StringComparison.Ordinal)) +
                                                ".hash",
                                                FileMode.OpenOrCreate, FileAccess.Write)))
                                {
                                    storeStream.WriteLine(digest.ToString());
                                }
                            this.Dispatcher.Invoke(delegate { button.IsEnabled = true; });
                        }
                    });
                }
                else if (RadioButtonStringSourceSelector.IsChecked != null &&
                         RadioButtonStringSourceSelector.IsChecked.Value)
                {
                    var bytesStr = Encoding.ASCII.GetBytes(TextBoxStringInput.Text);
                    if (button == null) return;
                    button.IsEnabled = false;
                    ThreadPool.QueueUserWorkItem(delegate
                    {
                        var digest = Md5.Calculate(new MemoryStream(bytesStr));
                        this.Dispatcher.Invoke(delegate { this.ResultText.Text += "\n Hash: " + digest; });
                        if (hashStoreFlag)
                            using (
                                var storeStream =
                                    new StreamWriter(new FileStream(
                                        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/hash.hash",
                                        FileMode.OpenOrCreate,
                                        FileAccess.Write)))
                            {
                                storeStream.WriteLine(digest.ToString());
                            }
                        this.Dispatcher.Invoke(delegate { button.IsEnabled = true; });
                    });
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}