﻿#region

using System;

#endregion

namespace SWine.MD5
{
    public class ProgressEventArgs : EventArgs
    {
        public ProgressEventArgs(long blocksProcessed, float progress)
        {
            this.Time = DateTime.Now;
            this.BlocksProcessed = blocksProcessed;
            this.Progress = progress;
        }

        public DateTime Time { get; }
        public long BlocksProcessed { get; }
        public float Progress { get; }
    }

    public class StartEventArgs : EventArgs
    {
        public StartEventArgs(byte rounds, long blocksCount)
        {
            this.Time = DateTime.Now;
            this.Rounds = rounds;
            this.BlocksCount = blocksCount;
        }

        public DateTime Time { get; }
        public byte Rounds { get; }
        public long BlocksCount { get; }
    }

    public class FinishEventArgs : EventArgs
    {
        public FinishEventArgs(Digest inputMd5, Digest outputMd5, bool stopped = false)
        {
            this.Time = DateTime.Now;
            this.InputMd5 = inputMd5;
            this.OutputMd5 = outputMd5;
            this.Stopped = stopped;
        }

        public bool Stopped { get; }
        public DateTime Time { get; }
        public Digest InputMd5 { get; }
        public Digest OutputMd5 { get; set; }
        public Digest OutpuMd5 { get; set; }
    }
}