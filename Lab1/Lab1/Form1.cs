﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab1.Abstract;
using Lab1.Helpers;
using Lab1.Models;

namespace Lab1
{
    public partial class Form1 : Form
    {
        private readonly string _filePath = FilePath.Result;

        private IPseudoRandom _pseudoRandom;

        public Form1()
        {
            InitializeComponent();
            this._pseudoRandom = new PseudoRandom(PseudoDatas.Get);

            UiDefault();
        }

        private void UiDefault()
        {
            UiTextBoxDefault();

            for (var i = 0; i < PseudoDatas.Count; i++)
            {
                this.cbVariant.Items.Add($"№{(i + 1)}");
            }
            this.cbVariant.SelectedIndex = PseudoDatas.Me;
        }

        private void UiTextBoxDefault()
        {
            this.tbxM.Text = this._pseudoRandom.Variant.M.ToString();
            this.tbxA.Text = this._pseudoRandom.Variant.A.ToString();
            this.tbxC.Text = this._pseudoRandom.Variant.C.ToString();
            this.tbxX.Text = this._pseudoRandom.Variant.X0.ToString();
        }

        private async void btnGenerate_Click(object sender, EventArgs e)
        {
            this.richTextBox.Text = "";
            this._pseudoRandom = new PseudoRandom(this._pseudoRandom.Variant);
            if (this._pseudoRandom.IsCalled())
                this._pseudoRandom.Reset();

            var count = this.numericUpDown.Value > 2147483647 ? 2147483647 : this.numericUpDown.Value;
            try
            {
                using (var sourceStream = new FileStream(this._filePath,
                 FileMode.Create, FileAccess.Write, FileShare.None,
                 4096, true))
                {
                    var firstNum = this._pseudoRandom.GetNext().ToString();
                    var current = "";
                    int i = 0;
                    while (firstNum != current)
                    {
                        current = this._pseudoRandom.GetNext().ToString();
                        if ((i + 1) % 1000000 == 0)
                            current += '#';
                        i++;

                        var encodedText = Encoding.UTF8.GetBytes($"{current}; ");
                        sourceStream.Write(encodedText, 0, encodedText.Length);
                    }

                    /*for (var i = 0; i < count; i++)
                    {
                        var s = this._pseudoRandom.GetNext().ToString();
                        if (i + 1 % 1000000 == 0)
                            s += '#';

                        //this.richTextBox.Text += await Task.Run(() => $"{s}; ");
                        //this.richTextBox.SelectionStart = richTextBox.Text.Length;
                        //this.richTextBox.ScrollToCaret();
                        //this.tspbPeriod.Value = await Task.Run(() => i1 * 100 / decimal.ToInt32(count));

                        var encodedText = Encoding.Unicode.GetBytes($"{s}; ");
                        sourceStream.Write(encodedText, 0, encodedText.Length);
                    }*/
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), @"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            this.tspbPeriod.Value = 100;
            await NotificationAsync("Згенеровано", 3);
        }

        private async Task NotificationAsync(string text, int sec)
        {
            await Task.Run(() =>
            {
                this.notifyIcon.BalloonTipText = text;
                this.notifyIcon.ShowBalloonTip(sec);
            });
        }

        private void btnPeriod_Click(object sender, EventArgs e)
        {
            if (this._pseudoRandom.IsCalled())
                this._pseudoRandom.Reset();
            var firstNum = this._pseudoRandom.GetNext();
            var period = 1;

            while (firstNum != this._pseudoRandom.GetNext())
                period++;

            this.lblPeriod.Text = $@"Період: {period}";
        }

        private void button_edit_var_Click(object sender, EventArgs e)
        {
            int m, a, c, x;
            try
            {
                m = int.Parse(this.tbxM.Text);
                a = int.Parse(this.tbxA.Text);
                c = int.Parse(this.tbxC.Text);
                x = int.Parse(this.tbxX.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), @"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;
            }

            if (CheckData(m, a, c, x))
                return;

            this._pseudoRandom = new PseudoRandom(new PseudoData(m, a, c, x));
        }

        private static bool CheckData(int m, int a, int c, int x)
        {
            if (!(m > 0))
            {
                MessageBox.Show(@"Модуль порівння повинен бути > 0", @"Помилка", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return true;
            }
            if (!(a >= 0 && a < m))
            {
                MessageBox.Show(@"Множник повинен бути > 0 і < m", @"Помилка", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return true;
            }
            if (!(c >= 0 && c < m))
            {
                MessageBox.Show(@"Приріст повинен бути > 0 і < m", @"Помилка", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return true;
            }
            if (!(x >= 0 && x < m))
            {
                MessageBox.Show(@"Початкове число повинен бути > 0 і < m", @"Помилка", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return true;
            }
            return false;
        }

        private void tbxM_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        private void cbVariant_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._pseudoRandom = new PseudoRandom(PseudoDatas.GetBy(this.cbVariant.SelectedIndex));
            UiTextBoxDefault();
        }
    }
}