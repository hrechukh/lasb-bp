﻿using System.Collections.Generic;
using Lab1.Models;

namespace Lab1.Helpers
{
    internal static class PseudoDatas
    {
        public const int Me = 5;

        private static readonly List<PseudoData> Elements = new List<PseudoData>
        {
            new PseudoData(2, 10, 1, 2, 5, 0, 2),
            new PseudoData(2, 11, 1, 3, 5, 1, 4),
            new PseudoData(2, 12, 1, 4, 5, 2, 8),
            new PseudoData(2, 13, 1, 5, 5, 3, 16),
            new PseudoData(2, 14, 1, 6, 5, 5, 32),
            new PseudoData(2, 15, 1, 2, 3, 8, 64),
            new PseudoData(2, 16, 1, 3, 3, 13, 128),
            new PseudoData(2, 17, 1, 4, 3, 21, 256),
            new PseudoData(2, 18, 1, 5, 3, 34, 512),
            new PseudoData(2, 19, 1, 6, 3, 55, 1024),
            new PseudoData(2, 20, 1, 7, 3, 89, 1),
            new PseudoData(2, 21, 1, 8, 3, 144, 3),
            new PseudoData(2, 22, 1, 9, 3, 233, 5),
            new PseudoData(2, 23, 1, 10, 3, 377, 7),
            new PseudoData(2, 24, 1, 11, 3, 610, 9),
            new PseudoData(2, 25, 1, 12, 3, 987, 11),
            new PseudoData(2, 26, 1, 13, 3, 1597, 13),
            new PseudoData(2, 27, 1, 14, 3, 2584, 17),
            new PseudoData(2, 28, 1, 15, 3, 4181, 19),
            new PseudoData(2, 29, 1, 16, 3, 6765, 23),
            new PseudoData(2, 30, 1, 17, 3, 10946, 29),
            new PseudoData(2, 31, 1, 7, 5, 17711, 31),
            new PseudoData(2, 31, 0, 2, 16, 28657, 33),
            new PseudoData(2, 31, 3, 2, 15, 46368, 37),
            new PseudoData(2, 31, 7, 2, 14, 75025, 41)
        };

        public static PseudoData GetBy(int index) => Elements[index];

        public static PseudoData Get => Elements[Me];

        public static int Count => Elements.Count;

    }
}