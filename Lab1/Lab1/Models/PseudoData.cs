﻿using System;

namespace Lab1.Models
{
    internal sealed class PseudoData
    {
        public PseudoData()
        {
        }

        public PseudoData(long m, long a, long c, long x0)
        {
            this.M = m;
            this.A = a;
            this.C = c;
            this.X0 = x0;
            this.X = x0;
        }

        public PseudoData(int num1, int pow1, int minus, int num2, int pow2, int growth, int x)
        :
            this((long)(Math.Pow(num1, pow1) - minus), (long)Math.Pow(num2, pow2), growth, x){}

        public long M { get; set; }
        public long A { get; set; }
        public long C { get; set; }
        public long X0 { get; set; }
        public long X { get; set; }
    }
}