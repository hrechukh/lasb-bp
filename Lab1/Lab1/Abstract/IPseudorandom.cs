﻿using Lab1.Models;

namespace Lab1.Abstract
{
    internal interface IPseudoRandom
    {
        PseudoData Variant { get; set; }
        long Current { get; }
        long GetNext();
        bool IsCalled();
        void Reset();
    }
}