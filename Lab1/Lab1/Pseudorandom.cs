﻿using Lab1.Abstract;
using Lab1.Models;

namespace Lab1
{
    internal class PseudoRandom : IPseudoRandom
    {
        private bool _x = true;
        public PseudoRandom(PseudoData variant)
        {
            this.Variant = variant;
        }

        public PseudoData Variant { get; set; }

        public long Current => this.Variant.X;

        public long GetNext()
        {
            if (this._x)
            {
                this._x = false;
                return this.Variant.X0;
            }
            this.Variant.X = (this.Variant.A * this.Variant.X + this.Variant.C) % this.Variant.M;
            return this.Current;
        }

        public bool IsCalled()
        {
            return this.Variant.X != this.Variant.X0;
        }

        public void Reset()
        {
            this._x = true;
            this.Variant.X = this.Variant.X0;
        }
    }
}